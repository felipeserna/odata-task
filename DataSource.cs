﻿namespace ODataWebAPI
{
    public static class DataSource
    {
        private static IList<Book> _books { get; set; }

        public static IList<Book> GetBooks()
        {
            if (_books != null)
            {
                return _books;
            }

            _books = new List<Book>();

            // book #1
            Book book = new Book
            {
                Id = 1,
                ISBN = "978-0-321-87758-1",
                Title = "Essential C#5.0",
                Author = "Mark Michaelis",
                Price = 59.99m,
                Location = new Address { City = "Redmond", Street = "156TH AVE NE" },
                Press = new Press
                {
                    Id = 1,
                    Name = "Addison-Wesley",
                    Category = Category.Book,
                    Email = "a@gmail.com"
                }
            };
            _books.Add(book);

            // book #2
            book = new Book
            {
                Id = 2,
                ISBN = "063-6-920-02371-5",
                Title = "Enterprise Games",
                Author = "Michael Hugos",
                Price = 49.99m,
                Location = new Address { City = "Bellevue", Street = "Main ST" },
                Press = new Press
                {
                    Id = 2,
                    Name = "O'Reilly",
                    Category = Category.EBook,
                    Email = "o@gmail.com"
                }
            };
            _books.Add(book);

            return _books;
        }
        // Companies
        private static IList<Company> _companies { get; set; }

        public static IList<Company> GetCompanies()
        {
            if (_companies != null)
            {
                return _companies;
            }

            _companies = new List<Company>();

            // company #1
            Company company = new Company
            {
                Id = 1,
                Name = "Pirelli",
                Rating = 40
            };
            _companies.Add(company);

            // company #2
            company = new Company
            {
                Id = 2,
                Name = "Goodyear",
                Rating = 67
            };
            _companies.Add(company);

            return _companies;
        }
    }
}
