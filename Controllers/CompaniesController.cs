﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;

namespace ODataWebAPI.Controllers
{
    public class CompaniesController : ODataController
    {
        private BookStoreContext _db;

        public CompaniesController(BookStoreContext context)
        {
            _db = context;
            if (context.Companies.Count() == 0)
            {
                foreach (var b in DataSource.GetCompanies())
                {
                    context.Companies.Add(b);
                }
                context.SaveChanges();
            }
        }


        // Return all companies
        [EnableQuery]
        public IActionResult Get()
        {
            return Ok(_db.Companies);
        }

        // Returns a specific company given its name
        /*[EnableQuery]
        public IActionResult Get(string key)
        {
            return Ok(_db.Companies.FirstOrDefault(c => c.Name == key));
        }*/

        // Create a new company
        [EnableQuery]
        public IActionResult Post([FromBody] Company company)
        {
            _db.Companies.Add(company);
            _db.SaveChanges();
            return Created(company);
        }
    }
}
